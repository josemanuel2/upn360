import styles from './layout.module.css'
import Link from 'next/link'
//Layout
class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {menuOpen:false}
  }
  onPressMenu(){
    this.setState({menuOpen:!this.state.menuOpen});
  }
  render() {
    return (<div id="pagina">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet"/>
      <header id="header">
        <div className="header_container">
        <Link href="/"><div className="logo"></div></Link>
        <div className="top_btn_content">
        <a href="#" className="top_btn">Más información</a>
        <a href="#" className="top_btn">POSTULA AQUÍ</a>
        </div>
        { !this.state.menuOpen &&
          <div className="menu" onClick={()=>{this.onPressMenu()}}></div>
        }
        { this.state.menuOpen &&
          <div className="menu_close" onClick={()=>{this.onPressMenu()}}></div>
        }
        </div>
      </header>
      <main id="main">
        { this.state.menuOpen &&
          <div className="mobile_list_menu">
            <ul>
            <li><a href="#">INICIO</a></li>
            <li><a href="#">NUESTROS CAMPUS</a></li>

            <ul className="list_campus">
            <li><Link href="/campus"><a>> Breña</a></Link></li>
            <li><Link href="/campus"><a>> Chorrillos</a></Link></li>
            <li><Link href="/campus"><a>> Comas</a></Link></li>
            <li><Link href="/campus"><a>> Los Olivos</a></Link></li>
            <li><Link href="/campus"><a>> San Juan de Lurigancho</a></Link></li>
            <li><Link href="/campus"><a>> Trujillo - El Molino</a></Link></li>
            <li><Link href="/campus"><a>> Trujillo - San Isidro</a></Link></li>
            <li><Link href="/campus"><a>> Cajamarca</a></Link></li>
            </ul>

            </ul>
          </div>
        }
        {this.props.children}
      </main>
      <footer id="footer">
        <p>Universidad Privada del Norte - Todos los derechos reservados</p>
      </footer>
    </div>);
  }
}
//export default Layout({ children })
export default Layout;
