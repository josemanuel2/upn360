import styles from './pop.module.css'
import Map from './map'
import { useRouter } from 'next/router'
//{this.state.tipo="texto" &&
//          {this.props.tipo="texto" &&



class ListElement extends React.Component {
  constructor(props) {
    super(props);
      this.state = {over:(Number(this.props.currentAmb)==Number(this.props.id))?true:false,current:(Number(this.props.currentAmb)==Number(this.props.id))?true:false}
      this.onMouseOver = this.onMouseOver.bind( this );
      this.onMouseLeave = this.onMouseLeave.bind( this );
      this.onClick = this.onClick.bind( this );
      if(Number(this.props.currentAmb)==Number(this.props.id)){

      }
  }
  onMouseOverE() {
    this.setState({over:true});
  }
  onMouseLeaveE(){
    if(!this.state.current){
      this.setState({over:false});
    }
  }
  onMouseOver() {
    this.props.onMouseOver(this.props.id);
    this.setState({over:true});
  }
  onClick(){
    this.props.onClick(this.props.id);
  }
  onMouseLeave(){
    this.props.onMouseLeave(this.props.id);
    if(!this.state.current){this.setState({over:false});}
  }
  render(){
    return (<li ref="elemento" className={(this.state.over)?styles.icoLeftSelected:''} onClick={this.onClick} onMouseOver={this.onMouseOver} onMouseLeave={this.onMouseLeave}>
      <span className={styles.icoLeft}>> </span>{this.props.texto}
    </li>);
  }
}

class Pop extends React.Component {
  constructor(props) {
    super(props);
    this.mapa = React.createRef();
    this.state = {tipo:props.tipo,currentAmb:0};
    //const router = useRouter()
  }
  onListOver(id){
    this.refs['item'+id].onMouseOverE();
  }
  onListOverE(id){
    //console.log("id "+id);
    //this.refs.listado.scrollIntoView({ behavior: 'smooth', block: 'start' })
    console.log(this.refs['item'+id].refs['elemento']);
    this.refs['item'+id].refs['elemento'].scrollIntoView({ behavior: 'smooth', block: 'start' });
    //this.refs.listado.scrollTop = 200;
    //console.log(this.refs['item'+id]);
    //this.refs['item'+id].scrollIntoView({ behavior: 'smooth', block: 'start' })
    //this.refs.listado.scroll(0,100);
    //this.refs.listado.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' })
  }
  outListOver(id){
    this.refs['item'+id].onMouseLeaveE();
  }
  onAmbienteOver(id){

    this.refs.mapa.overList(id);
  }
  onAmbienteOut(id){
    this.refs.mapa.outList(id);
  }
  onClickAmbiente(id){
    console.log("onPressAmbiente "+id);
  }
  render() {
    const elements = ['one', 'two', 'three','one', 'two', 'three','one', 'two', 'three','one', 'two', 'three','one', 'two', 'three','one', 'two', 'three', 'three'];
    return (
      <div className={(this.state.tipo=="mapa"?styles.container_mapa:styles.container)}>
          <div className={styles.header}>
            <div className={styles.iconClose} onClick={()=>{this.props.close();}}></div>
          </div>
          {this.state.tipo=="texto" &&
          <div className={styles.contentText}>
          Somos una universidad licenciada por SUNEDU. Contamos con la acreditación internacional IAC-SINDA y con programas acreditados por ICACIT y SINEACE. Ademas, tenemos alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.
          </div>}
          {this.state.tipo=="imagen" &&
          <div className={styles.contentText}>
          <img src="/img/pic1.jpg"/>
          9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso.
          </div>}
          {this.state.tipo=="mapa" &&
          <div className={styles.contentPop}>
            <div className={styles.contentPopLeft}>
              <Map  currentAmb={this.state.currentAmb}
                    ref="mapa"
                    className={styles.map}
                    onClick={(id)=>{ this.onClickAmbiente(id);}}
                    overItem={(id)=>{ this.onListOver(id);}}
                    overItemE={(id)=>{ this.onListOverE(id);}}
                    outItem={(id)=>{this.outListOver(id);}}>
              </Map>
              <div className={styles.btnTourAutomatico}>
                <div className={styles.icoPlay}></div> realizar tour automático de toda la facultad
              </div>
            </div>
            <div className={styles.contentPopRight}>
              <div className={styles.contentAmbientes}>
                <h4>AMBIENTES</h4>
                <div className={styles.listadoAmbientes}>
                  <ul ref={"listado"}>
                  {elements.map((value, index) => {
                    return <ListElement currentAmb={this.state.currentAmb} ref={"item"+(index+1)} key={index} id={index+1} texto={value}
                    onMouseOver={(i)=>{this.onAmbienteOver(i)}}
                    onMouseLeave={(i)=>{this.onAmbienteOut(i)}}
                    onClick={(i)=>{this.onClickAmbiente(i)}}
                    />
                  })}
                  </ul>
                </div>
              </div>
            </div>
          </div>}
        </div>
    );
  }
}
//export default Pop;

export default Pop


/*
export default function Pop({ children }) {
  return <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.iconClose}></div>
      </div>
      {this.state.tipo="texto" &&
      <div className={styles.contentText}>
      Somos una universidad licenciada por SUNEDU. Contamos con la acreditación internacional IAC-SINDA y con programas acreditados por ICACIT y SINEACE. Ademas, tenemos alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.
      </div>
      }

  </div>
}
*/
