import Layout from '../components/layout'
import Visor360 from '../components/visor360'
import Pop from '../components/pop'
export default function Visor() {
  return <Layout>
          <h1>Visor</h1>
          <Visor360></Visor360>
          <Pop tipo="texto"></Pop>
          <Pop tipo="imagen"></Pop>
          <Pop tipo="mapa"></Pop>
        </Layout>
}
