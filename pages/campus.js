import Layout from '../components/layout'
import Pop from '../components/pop'
import styles from './campus.module.css'

class Campus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false,showAmbientes:true,showButtons:true,showFoto:false}
  }
  verInformacion(){
    this.setState({showInfo:true,showButtons:false});
  }
  verAmbientes(){
    this.setState({showAmbientes:true,showButtons:false});
  }
  verFoto(){
    this.setState({showFoto:true,showButtons:false});
  }

  render(){
    return (<Layout>
              <div className={styles.visor}>

              <div className={styles.tituloContainer}>
                <div className={styles.titulo}>
                  <div className={styles.triangulo}></div>
                  <h1>San Juan de Lurigancho</h1>
                </div>
                <div className={styles.titulo_bottom}>
                  <div className={styles.titulo_bottom_yellow}></div>
                </div>
              </div>
              <div className={styles.layer1}>
                VISOR 360
              </div>
              <div className={styles.layer2}>

                { this.state.showButtons &&
                  <div className={styles.controles_medio}>
                    <div className={styles.controles_atras_adelante}>
                      <div className={styles.icono+ ' ' + styles.iconoLeft}><div></div></div>
                      <div className={styles.icono+ ' ' + styles.iconoRight}><div></div></div>
                    </div>
                  </div>
                }
                { !this.state.showButtons &&
                  <div className={styles.controles_medio}></div>
                }

                { this.state.showButtons &&
                  <div className={styles.controles_video}>
                    <div className={styles.icono+ ' ' +styles.iconoVerFoto} onClick={()=>{this.verFoto();}}><div></div></div>
                    <div style={{width:30}}></div>
                    <div className={styles.icono+ ' ' +styles.iconoVerVideo}><div></div></div>
                    <div style={{width:30}}></div>
                    <div className={styles.icono+ ' ' +styles.iconoVerInformacion} onClick={()=>{this.verInformacion();}}><div></div></div>
                  </div>
                }
                <div className={styles.controles}>
                  <div className={styles.mainControl}>
                    <div className={styles.icono+ ' ' +styles.iconoVerCampus}><div></div></div>
                    <div className={styles.icono+ ' ' +styles.iconoPlay}><div></div></div>
                    <div className={styles.icono+ ' ' +styles.iconoVerUbicacion} onClick={()=>{this.verAmbientes();}}><div></div></div>
                  </div>
                </div>

              </div>
              { this.state.showInfo &&
              <div className={styles.layer3}>
                <Pop tipo={"texto"} close={()=>{this.setState({showInfo:false,showButtons:true});}}></Pop>
              </div>
              }
              { this.state.showAmbientes &&
              <div className={styles.layer3}>
                <Pop tipo={"mapa"} close={()=>{this.setState({showAmbientes:false,showButtons:true});}}></Pop>
              </div>
              }
              { this.state.showFoto &&
              <div className={styles.layer3}>
                <Pop tipo={"imagen"} close={()=>{this.setState({showFoto:false,showButtons:true});}}></Pop>
              </div>
              }

              </div>
            </Layout>);
  }

}
export default Campus;
